const requiredURL = 'https://reqres.in/api/users?per_page=12';

export const setEmployees = async () => {
  const response = await fetch(requiredURL, {
    method: 'GET',
    headers: {
      'Accept': 'application/json',
    },
  });

  const data = await response.json();
  return data;
};