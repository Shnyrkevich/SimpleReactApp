import { actionCreator } from '../actions';
import { setEmployees } from '../../api/services';

export const setEmploeesThunkCreator = () => (dispatch) => {
  dispatch(actionCreator().toggleLoadStatus(true));
  setEmployees()
  .then(response => {
    dispatch(actionCreator().setEmployees(response.data));
    dispatch(actionCreator().toggleLoadStatus(false));
  })
};