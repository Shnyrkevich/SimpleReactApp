export const types = {
  SET_EMPLOYEES: 'SET_EMPLOYEES',
  TOGGLE_LOAD_STATUS: 'TOGGLE_LOAD_STATUS',
  ADD_EMPLOYEE: 'ADD_EMPLOYEE',
  DELETE_EMPLOYEE: 'DELETE_EMPLOYEE',
};

export const actionCreator = () => {
  return {
    setEmployees: (employers) => ({ type: types.SET_EMPLOYEES, data: employers }),
    toggleLoadStatus: (status) => ({ type: types.TOGGLE_LOAD_STATUS, data: status }),
    addNewEmployee: (user) => ({ type: types.ADD_EMPLOYEE, data: user }),
    deleteEmployee: (id) => ({ type: types.DELETE_EMPLOYEE, data: id }),
  }
};