import{ applyMiddleware, combineReducers, createStore } from 'redux';
import thunk from 'redux-thunk';
import employeesReducer from './reducers/employees-reducer';

let store = createStore(employeesReducer, applyMiddleware(thunk));

window.store = store;

export default store;