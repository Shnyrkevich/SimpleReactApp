import { types } from '../actions';

const initialState = {
  employeesPage: {
    employees: null,
    isLoad: false,
  },
};

const employeesReducer = (state = initialState, action) => {
  switch(action.type) {
    case types.SET_EMPLOYEES: {
      return {
        ...state,
        employeesPage: {
          ...state.employeesPage,
          employees: action.data,
        }
      };
    }
    case types.TOGGLE_LOAD_STATUS: {
      return {
        ...state,
        employeesPage: {
          ...state.employeesPage,
          isLoad: action.data,
        }
      };
    }
    case types.ADD_EMPLOYEE: {
      let user = {
        ...action.data,
        id: state.employeesPage.employees.length + 1
      };
      return {
        ...state,
        employeesPage: {
          ...state.employeesPage,
          employees: [...state.employeesPage.employees, user],
        }
      };
    }
    case types.DELETE_EMPLOYEE: {
      return {
        ...state,
        employeesPage: {
          ...state.employeesPage,
          employees: state.employeesPage.employees.filter(el => el.id !== action.data),
        }
      };
    }
    default: {
      return state;
    } 
  }
};

export default employeesReducer;