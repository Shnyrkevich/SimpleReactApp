import './App.css';
import { Route } from 'react-router-dom';
import Navigation from '../Navigation/Navigation';
import MainPage from '../MainPage/MainPage';
import EmployeesContainer from '../EmployeesPage/Employees';

function App() {
  return (
    <div className='wrapper'>
      <Navigation />
      <div className='main-content'>
        <Route exact path='/' render={() => <MainPage />} />
        <Route path='/employees' render={() => <EmployeesContainer />} />
      </div>
    </div>
  );
}

export default App;
