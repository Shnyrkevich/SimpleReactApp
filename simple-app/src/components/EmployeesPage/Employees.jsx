import React, { useEffect, useRef } from 'react';
import { connect } from 'react-redux';
import { setEmploeesThunkCreator } from '../../store/thunkReducers/employeesThunkReducer';
import { actionCreator } from '../../store/actions';
import Preloader from '../Preloader/Preloader';
import './employees.css';

const Employ = (props) => {
  return (
    <div className='employee'>
      <div className='employee-name'>{`${props.first_name} ${props.last_name}`}</div>
      <button className="btn" onClick={() => props.deleteEmployee(props.id)}>Delete Employee</button>
    </div>
  );
};

const Employees = (props) => {
  const firstNameRef = useRef();
  const secondNameRef = useRef();

  function createEmployee() {
    if (firstNameRef.current.value && secondNameRef.current.value) {
      let employee = {
        first_name: firstNameRef.current.value,
        last_name: secondNameRef.current.value,
      };
      props.addNewEmployee(employee);
      firstNameRef.current.value = '';
      secondNameRef.current.value = '';
    }
  }

  return props.employees ?
    <div className="employees-container">
      <div className='employees-container_employees'>
        {
          props.employees.map(el => <Employ {...el} deleteEmployee={props.deleteEmployee} key={el.id}/>)
        }
      </div>
      <div className="employees-container_add-form">
        <input ref={firstNameRef} type='text' placeholder='Input first-name' required/>
        <input ref={secondNameRef} type='text' placeholder='Input last-name' required/>
        <button className='btn' onClick={() => createEmployee()}>Add new employee</button>
      </div>
    </div> :
    null;
};

const EmployeesContainer = (props) => {
  useEffect(() => {
    if (!props.employees) {
      props.setEmployees();
    }
  }, [props.employees]);

  return props.isLoad ? <Preloader /> : <Employees {...props} />
}

const mapStateToProps = (state) => {
  return {
    isLoad: state.employeesPage.isLoad,
    employees: state.employeesPage.employees,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setEmployees: () => dispatch(setEmploeesThunkCreator()),
    addNewEmployee: (employee) => dispatch(actionCreator().addNewEmployee(employee)),
    deleteEmployee: (id) => dispatch(actionCreator().deleteEmployee(id)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EmployeesContainer);