import React from 'react';
import preloaderWay from '../../assets/images/810.svg';
import './preloader.css';

const Preloader = () => {
  return (
    <div className='preloader'>
      <img src={preloaderWay} alt="preloader image" className='preloader-image'/>
    </div>
  );
};

export default Preloader;