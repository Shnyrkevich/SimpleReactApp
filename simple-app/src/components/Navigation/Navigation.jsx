import React from 'react';
import './navigation.css';
import { NavLink } from 'react-router-dom';

const Navigation = () => {
  return (
    <div className="navigation">
      <nav>
        <NavLink to='/'>Главная</NavLink>
        <NavLink to='/employees'>Сотрудники</NavLink>
      </nav>
    </div>
  );
};

export default Navigation;