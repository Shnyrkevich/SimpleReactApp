import React from 'react';

const MainPage = () => {
  return (
    <div className='main-page-container'>
      <h2 className="some-text">Hello, this is study react-app)</h2>
    </div>
  );
};

export default MainPage;